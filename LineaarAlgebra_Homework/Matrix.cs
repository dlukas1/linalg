﻿using System;

namespace LineaarAlgebra_Homework
{
    public class Matrix
    {
        private double[][] matrix;
        private int dimensions; // number of rows and columns

        public Matrix(double[][] dArray)
        {
            matrix = dArray;
            dimensions = matrix.Length;

            // Check if matrix is square
            for (int i = 0; i < dimensions; i++)
            {
                if (matrix.Length != dimensions)
                {
                    Console.WriteLine("Matrix is not square!");
                    Console.ReadLine();
                    break;
                }
            }
        }

        public double findDeterminant()
        {
            // if matrix size is 1x1
            if (dimensions == 1)
            {
                return matrix[0][0];
            }
            // else recursive call to submatrix
            double determinant = 0;
            for (int i = 0; i < dimensions; i++)
            {
                if (i % 2 == 0)
                {
                    determinant += matrix[0][i] * getSubmatrix(this, 0, i).findDeterminant();
                }
                else
                {
                    determinant -= matrix[0][i] * getSubmatrix(this, 0, i).findDeterminant();
                }
            }
            return determinant;
        }

        private Matrix getSubmatrix(Matrix m, int rowToExclude, int columnToExclude)
        {
            double[][] values = new double[m.dimensions - 1][];
            for (int i = 0; i < m.dimensions; i++)
            {
                for (int j = 0; j < m.dimensions; j++)
                {
                    if (i != rowToExclude && j != columnToExclude)
                    {
                        values[i < rowToExclude ? i : i - 1][j < columnToExclude ? 
                            j : j - 1] = m.matrix[i][j];
                    }
                }
            }
            return new Matrix(values);
        }
    }
}
