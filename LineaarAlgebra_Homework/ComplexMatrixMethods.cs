﻿using System;

namespace LineaarAlgebra_Homework
{
    public static class ComplexMatrixMethods
    {
        public static ComplexMatrix getSubmatrix(ComplexMatrix m, int columnToExclude)
        {
            // will always exclude upper row 
            Complex[,] sub = new Complex[m.dimensions - 1, m.dimensions - 1];

            for (int i = 0; i < m.dimensions - 1; i++)
            {
                for (int j = 0; j < m.dimensions - 1; j++)
                {
                    sub[i, j] = m.matrix[i+1, ( j < columnToExclude)? j : j+1];             
                }
            }
            return new ComplexMatrix(sub);
        }

        public static ComplexMatrix replaceColumn(ComplexMatrix m, int columnToReplace, Complex[] replacements)
        {
            for (int i = 0; i < m.dimensions; i++)
            {
                for (int j = 0; j < m.dimensions; j++)
                {
                    if(j == columnToReplace)
                    {
                        m.matrix[i, j] = replacements[i];
                    }
                }
            }
            return m;
        }

        public static void SolveEquation(ComplexMatrix m, Complex[]arr)
        {
            Complex divider = calcDeterminant(m);
            if(divider.isNull())
            {
                Console.WriteLine("Can't divide to 0");
                return;
            }
           
                for (int i = 0; i < m.dimensions; i++)
                {
                    ComplexMatrix clone = m.Clone();                  
                    replaceColumn(clone, i, arr);
                    Complex res1 = calcDeterminant(clone);
                    Complex resFinal = ComplexMethods.Divide( res1, divider );
                    Console.WriteLine($"X{i+1} = {resFinal.toString()}");
                    clone = null;
                }
        }

        public static Complex calcDeterminant(ComplexMatrix cm)
        {
            if (cm.dimensions == 1) return cm.matrix[0, 0];

            else if (cm.dimensions == 2)
            {
                // |a   b|
                // |c   d| => d = a*d-c*d
                Complex r1 = ComplexMethods.Multiply(cm.matrix[0, 0], cm.matrix[1, 1]);
                Complex r2 = ComplexMethods.Multiply(cm.matrix[0, 1], cm.matrix[1, 0]);
                return ComplexMethods.Minus(r1, r2);
            }
            else
            {
                Complex result = new Complex(0, 0);
                for (int i = 0; i < cm.dimensions; i++)
                {
                    result = (i % 2 == 0) ? ComplexMethods.Plus(result, ComplexMethods.Multiply(
                       cm.matrix[0, i], calcDeterminant(ComplexMatrixMethods.getSubmatrix(cm, i)))) :
                       ComplexMethods.Minus(result, ComplexMethods.Multiply(
                       cm.matrix[0, i], calcDeterminant(ComplexMatrixMethods.getSubmatrix(cm, i))));
                }
                return result;
            }
        }
    }
}
