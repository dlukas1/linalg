﻿namespace LineaarAlgebra_Homework
{
    public class Complex
    {
        // (5 + 3i)
        public double Re { get; set; } // real part
        public double Im { get; set; } // imaginary part

        public Complex (double re, double im)
        {
            Re = re;
            Im = im;
        }
        public Complex() {}
        
        public string toString()
        {
            if (Re == 0 && Im == 0) return "0"; // if 0+oi then 0

            else if (Re == 0) return $"{Im}i";  
            
            else if (Im == 0) return $"{Re}";
          
            else if (Im < 0) return $"{Re} {Im}i";  // 3 -2i

            else return $"{Re} + {Im}i";
        }

        public bool isNull()
        {
            return (Re == 0 && Im == 0) ? true : false;
        }
    }
}
