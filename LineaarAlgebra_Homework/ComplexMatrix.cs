﻿using System;

namespace LineaarAlgebra_Homework
{
    public class ComplexMatrix
    {
        public Complex [,] matrix;
        public int dimensions;

        public ComplexMatrix(Complex[,] cArray)
        {
            matrix = cArray;
            dimensions = (int)Math.Sqrt(matrix.Length);
        }
        public ComplexMatrix() { }


        public ComplexMatrix Clone()
        {
            Complex[,] cloneArr = new Complex[dimensions, dimensions];
            for (int i = 0; i < dimensions; i++)
            {
                for (int j = 0; j < dimensions; j++)
                {
                    cloneArr[i, j] = matrix[i, j];
                }
            }
            return new ComplexMatrix(cloneArr);
        }


        public void printMatrix()
        {
            for (int i = 0; i < dimensions; i++)
            {
                string s = "";
                for (int j = 0; j < dimensions; j++)
                {
                    if(matrix[i,j] == null)
                    {
                        s += "|\tNULL\t|";
                    } else
                    {
                        s += "|\t" + matrix[i,j].toString() + "\t|";
                    }                   
                }
                Console.WriteLine(s);
            }
            Console.ReadLine();
        }     
    }
}