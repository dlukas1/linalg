﻿using System;

namespace LineaarAlgebra_Homework
{
    public static class ComplexMethods
    {
        public static Complex Plus(Complex a, Complex b)
        {
            return new Complex(a.Re + b.Re, a.Im + b.Im);
        }
        public static Complex Minus(Complex a, Complex b)
        {
            return new Complex(a.Re - b.Re, a.Im - b.Im);
        }

        //  (a+bi)*(c+di) = (ac-bd)+(ad+bc)i
        public static Complex Multiply(Complex a, Complex c)
        {
            double re = a.Re * c.Re - a.Im * c.Im;
            double im = a.Re * c.Im + a.Im * c.Re;
            return new Complex(re, im);
        }

        // (a+bi)/(c+di) = ((ac+bd)/(c^2+d^2)) + ((bc-ad)/(c^2+d^2))i
        public static Complex Divide(Complex a, Complex c)
        {
            double re = (a.Re * c.Re + a.Im * c.Im) / (c.Re * c.Re + c.Im * c.Im);
            double im = (a.Im * c.Re - a.Re * c.Im) / (Math.Pow(c.Re, 2) + Math.Pow(c.Im, 2));
            return new Complex(re, im);
        }
    }
}
