﻿namespace LineaarAlgebra_Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            //TEST 1
            //x+y = 5
            //x-y = -1

            Complex a = new Complex(1, 0);
            Complex b = new Complex(1, 0);
            Complex c = new Complex(1, 0);
            Complex d = new Complex(-1, 0);

            Complex[] arr = new Complex[] { new Complex(5, 0), new Complex(-1, 0) };


            Complex[,] cArray = new Complex[,] {
                    { a, b },
                    { c, d }};

            ComplexMatrix matrix = new ComplexMatrix(cArray);
            matrix.printMatrix();
            ComplexMatrixMethods.SolveEquation(matrix, arr);
            Console.ReadLine();
            */
            /*
            TEST 2
            // (1+i)X1 + (1-i)X2 = -1+i
            // (1+2i)X1 + (1-2i)X2 = -4+i
            Complex a = new Complex(1, 0);
            Complex b = new Complex(1, 0);
            Complex c = new Complex(1, 0);
            Complex d = new Complex(-1, 0);

            Complex[] arr = new Complex[] { new Complex(5, 0), new Complex(-1, 0) };


            Complex[,] cArray = new Complex[,] {
                    { a, b },
                    { c, d }};
                    
            ComplexMatrix matrix = new ComplexMatrix(cArray);
            matrix.printMatrix();
            ComplexMatrixMethods.SolveEquation(matrix, arr);
            Console.ReadLine();
            */

            /*
            // TEST 3
            // 3x + 2y + z = 10
            // x + y + z = 6
            // 2x + y - z = 1
            Complex a = new Complex(3, 0);
            Complex b = new Complex(2, 0);
            Complex c = new Complex(1, 0);

            Complex e = new Complex(1, 0);
            Complex f = new Complex(1, 0);
            Complex g = new Complex(1, 0);

            Complex h = new Complex(2, 0);
            Complex i = new Complex(1, 0);
            Complex j = new Complex(-1, 0);

            Complex[] arr = new Complex[] { new Complex(10, 0), new Complex(6, 0), new Complex(1, 0) };


            Complex[,] cArray = new Complex[,] {
                    { a, b , c},
                    { e, f, g },
                    { h, i, j } };

            ComplexMatrix matrix = new ComplexMatrix(cArray);
            matrix.printMatrix();
            ComplexMatrixMethods.SolveEquation(matrix, arr);
            Console.ReadLine();
            */

            /*
            // TEST 4
            // (1+2i)X1 + 3X2 + (2+i)X3 = 1+i
            // (1i)X1 + (1+2i)X2 + (2+3i)X3 = 2i
            // (8-i)X1 + (2-i)X2 + (-2i)X3 = 3+3i
            Complex a = new Complex(1, 2);
            Complex b = new Complex(3, 0);
            Complex c = new Complex(2, 1);

            Complex e = new Complex(0, 1);
            Complex f = new Complex(1, 2);
            Complex g = new Complex(2, 3);

            Complex h = new Complex(8, -1);
            Complex i = new Complex(2, -1);
            Complex j = new Complex(0, -2);

            Complex[] arr = new Complex[] { new Complex(1, 1), new Complex(0, 2), new Complex(3, 3) };


            Complex[,] cArray = new Complex[,] {
                    { a, b , c},
                    { e, f, g },
                    { h, i, j } };

            ComplexMatrix matrix = new ComplexMatrix(cArray);
            matrix.printMatrix();
            ComplexMatrixMethods.SolveEquation(matrix, arr);
            Console.ReadLine();
            */

            /*
            // TEST 5
            // 3x + 2y + z = 10
            // x + y + z = 6
            // 2x + y - z = 1
            Complex a = new Complex(3, 0);
            Complex b = new Complex(2, 0);
            Complex c = new Complex(1, 0);

            Complex e = new Complex(-1, 0);
            Complex f = new Complex(-1, 0);
            Complex g = new Complex(1, 0);

            Complex h = new Complex(1, 0);
            Complex i = new Complex(1, 0);
            Complex j = new Complex(-1, 0);

            Complex[] arr = new Complex[] { new Complex(10, 0), new Complex(0, 0), new Complex(0, 0) };


            Complex[,] cArray = new Complex[,] {
                    { a, b , c},
                    { e, f, g },
                    { h, i, j } };

            ComplexMatrix matrix = new ComplexMatrix(cArray);
            matrix.printMatrix();
            ComplexMatrixMethods.SolveEquation(matrix, arr);
            Console.ReadLine();
            */         
        }
    }
}
